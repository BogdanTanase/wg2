//Developed by Bogdan Tanase
//June-2024
//update July-2024

//load data
var proms = [];
var distr = [];
var distr2 = [];
var dataIsLoaded = false;
//const prom1 = setDistr(['https://gitlab.com/BogdanTanase/wordgen/-/raw/main/t222_1.gz'], distr);
//const prom2 = setDistr(['https://gitlab.com/BogdanTanase/wordgen/-/raw/main/t222_2.gz'], distr);
//const prom3 = setDistr(['https://gitlab.com/BogdanTanase/wordgen/-/raw/main/t222_3.gz'], distr);

async function loadData() {
	proms.push(setDistr(['t222_1.gz'], distr, 1));
	proms.push(setDistr(['t222_2.gz'], distr, 2));
	proms.push(setDistr(['t222_3.gz'], distr, 3));
	//

	//console.log('len:proms '+ proms.length);
	var p;
	p = await getDistr(['t222_4_1.gz'], distr2);
	p = await getDistr(['t222_4_2.gz'], distr2);
	p = await getDistr(['t222_4_3.gz'], distr2);
	p = await getDistr(['t222_4_4.gz'], distr2);
	p = await getDistr(['t222_4_5.gz'], distr2);
	p = await getDistr(['t222_4_6.gz'], distr2);
	p = await getDistr(['t222_4_7.gz'], distr2);
	p = Promise.allSettled([p]).then((result) => {
		//console.log('distr len:'+ distr.length);
		const d3 = new Distribution(distr2, 4);
		distr.push(d3);
		
		//distr2 = [];
		dataIsLoaded = true;
	});
	return p;
}

class Distribution {
	zero = String(0);
	constructor(datastring, id) {
		if (typeof datastring === "string") {
			this.datastring = String(datastring);
		}
		else if (typeof datastring === "object" && datastring.length > 0) {
			this.dataArray = datastring;
			this.dataArrayBegin = Array(datastring.length).fill(0);
		}
		this.id = id;
		//
		this.dataseparator = ',';
		this.datalineEndchar = '\n';
		//
		this.validItems = [];
		//
		this.itemPos = [];
		if (typeof this.dataArray !== 'undefined') {
			let i = 0;
			for (const elem of this.dataArray) {
				this.dataArrayBegin[i] = this.itemPos.length;
				this.initItemPos(elem);
				i++;
			}
		}
		else {this.initItemPos(this.datastring);}		
	}
	
	pushItem(b, e, data) {
		this.itemPos.push([b, e]);
		if (this.checkItem(b, e, data)) {
			this.validItems.push(this.itemPos.length - 1);
		}
	}
	
	initItemPos(data) {
		console.log('Initializing data for : ' + this.id);
		let b = 0;
		let e = data.length - 1;
		let p;
		let p0 = data.indexOf(this.datalineEndchar, b);
		if (p0 != -1) {
			this.pushItem(b, p0-1, data);
			b = p0 + 1;
			while (b <= e && e - b > p0) {
				p = data.indexOf(this.datalineEndchar, b);
				this.pushItem(b, p-1, data);
				b = p + 1;
			}
		}
		this.pushItem(b, e, data);
		console.log('\tFinished initializing data.');
	}

	getItem(itemNr) {
		let b = this.itemPos[itemNr][0];
		let e = this.itemPos[itemNr][1];
		
		if (typeof this.dataArray !== 'undefined') {	
			if (this.validItems.indexOf(itemNr) !== -1) {
				let n = -1;
			
				for (let i = this.dataArrayBegin.length - 1; i >= 0 ; i--) {
					if (itemNr >= this.dataArrayBegin[i]) {
						n = i;
						break;
					}
					
				}
			
				let data = this.dataArray[n].slice(b, e);
				if (this.checkItem(b, e, this.dataArray[n])) {
					let dArray = data.split(this.dataseparator);
					for (let i = 0; i < dArray.length; i++) {
						dArray[i] = Number(dArray[i]);
					}
					return dArray;
				}
			}
		}
		else {
			if (this.validItems.indexOf(itemNr) !== -1) {
				let data = this.datastring.slice(b, e);
				if (this.checkItem(b, e, this.datastring)) {
					let dataArray = data.split(this.dataseparator);
					for (let i = 0; i < dataArray.length; i++) {
						dataArray[i] = Number(dataArray[i]);
					}
					return dataArray;
				}
			}
		}
		return -1;
	}
	
	checkItem(b, e, data) {
		let d = data.slice(b, e);
		return this.stringContainsDecimals(d);
	}
	
	stringContainsDecimals(s) {
		if (typeof s === "string") {
			for (let i = 1; i < 10; i++) {
				if (s.search(i) >= 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	isValid() {return this.validItems.length > 0}
}

//from https://stackoverflow.com/questions/76691769/how-to-use-decompressionstream-to-decompress-a-gzip-file-using-javascript-in-bro
const decompress = async (url) => {
  const ds = new DecompressionStream('gzip');
  const response = await fetch(url);
  const blob_in = await response.blob();
  const stream_in = blob_in.stream().pipeThrough(ds);
  const blob_out = await new Response(stream_in).blob();
  return await blob_out.text();
};

async function getData(dataUrls) {
	let r = '';
	//avoid CORS err by using free online proxy site
	let url_proxy = 'https://corsproxy.io/?';
	url_proxy = '';/////////////////////////////proxy not needed now
	for (const url_data of dataUrls) {
		const url = url_proxy + encodeURIComponent(url_data);
		const result = await decompress(url);
		r += result;
	}
	return r;
}

function setDistr(urlArray, distr, id){
	let res = '';
	res = getData(urlArray);

	var data = '';
	res.then((result) => {data = result});
	
	return Promise.allSettled([res]).then((result) => {distr.push(new Distribution(data, id));});
}

function getDistr(urlArray, dArray){
	let res = '';
	res = getData(urlArray);

	var data = '';
	res.then((result) => {data = result});
	
	return Promise.allSettled([res]).then((result) => {dArray.push(data);});
}

export {dataIsLoaded, distr, loadData};
