//from https://stackoverflow.com/questions/65469293/create-a-row-in-an-airtable-base-from-static-html-form
//my mod
var lookupHeaders = new Headers();
var lookupRequestOptions = {
			  method: 'GET',
			  headers: lookupHeaders,
			  redirect: 'follow'
			};
var insertHeaders = new Headers();
insertHeaders.append("Content-Type", "application/json");

var url;

//5 requests per second ; per base
//10 records per request ; batch of 10
//50 requests per second for all traffic using personal access tokens from a given user or service account
var RATE_LIMIT_RS = 5;
var RATE_LIMIT_RR = 10;
var RATE_LIMIT_RTS = 50;

//
var MAX_URI_LNGTH = 2000;

async function init() {	
	let modc = await import('./air_const.js');
	Promise.allSettled([modc]).then((r) => {
		//console.log("module r:", r[0].value);
		
		let p1 = main(r[0].value);
		//Promise.allSettled([p1]).then(() => {console.log("url:", url);})
	});
	
}

async function main(module) {
	//console.log("module:", module, module.YOUR_API_KEY);
	
	lookupHeaders.append("Authorization", `Bearer ${module.YOUR_API_KEY}`);
	insertHeaders.append("Authorization", `Bearer ${module.YOUR_API_KEY}`);	
	url =  `https://api.airtable.com/v0/${module.YOUR_BASE_ID}/${module.YOUR_TABLE_ID}`;
}

function oldINSERT(word) {
	var data = {
	  fields: {
	    fldVbAKgn9005YZ0l : String(word)
	  }
	}
	
	var raw = JSON.stringify(data);

	var insertRequestOptions = {
	  method: 'POST',
	  headers: insertHeaders,
	  body: raw,
	  redirect: 'follow'
	};

	post(new Request(url, insertRequestOptions));
}

async function INSERT(wordsArray) {
	var data;
	var raw;
	var insertRequestOptions;
	let rqst;
	if (wordsArray.length > 0) {
		let recordsList = batch(wordsArray);
		if (recordsList.length > 0) {
			for (let i = 0; i < recordsList.length; i++) {
				data = {records: recordsList[i]};
				//console.log("data insert:", data);
				raw = JSON.stringify(data);

				insertRequestOptions = {
				  method: 'POST',
				  headers: insertHeaders,
				  body: raw,
				  redirect: 'follow'
				};
				
				rqst = await post(new Request(url, insertRequestOptions));
				//rqst = await setTimeout(post(new Request(url, insertRequestOptions)), 1200);
			}
		}
	}
}

async function oldLOOKUP(word) {
	let lookupUrl = url + `?filterByFormula=w%3D'${String(word)}'`;
	return await post(new Request(lookupUrl, requestOptions));
}

async function LOOKUP(wordsArray) {
	if (wordsArray.length === undefined || typeof wordsArray === 'string') {wordsArray = [wordsArray]}
	let mapFunction = async (wrdsArray) => {
		let lookupUrl = LOOKUP_buildURL(wrdsArray);
		return await post(new Request(lookupUrl, lookupRequestOptions));
		}
	
	let lookupUrl = LOOKUP_buildURL(wordsArray);
	
	if (! lookupUrl.length > MAX_URI_LNGTH) {		
		return await post(new Request(lookupUrl, lookupRequestOptions));
	}
	else {
		var out = [];
		let m = Math.ceil(lookupUrl.length / MAX_URI_LNGTH);
		let batchSize = Math.floor(wordsArray.length / m);
		
		let batchRsltsArray = batchAndMap(wordsArray, batchSize, mapFunction);
		let p;
		let promises = [];
		for (let i = 0; i < batchRsltsArray.length; i++) {
			p = batchRsltsArray[i].then((r) => {out.push(LOOKUP_processResultRecords(r.records));})
			promises.push(p)
		}
		return await Promise.allSettled(promises).then((r) => {
			out = arrayOfArrays2array(out);
			console.log('out',out);
			return out;
		});
	}
}

function LOOKUP_processResultRecords(recordsArray) {
	let foundWords = [];
	if (recordsArray.length !== 0) {
		let o1;
		let o2;
		for (let i = 0; i < recordsArray.length; i++) {
			o1 = recordsArray[i];
			o2 = o1.fields;
			foundWords.push(o2.w);
		}
	}
	return foundWords;
}

function LOOKUP_buildURL(wordsArray) {
	let filter;
	filter = wordsArray.map((x) => `w+%3D+'${String(x)}'`);
	filter = filter.reduce((x,y) => x+'%2C'+y);//===,
	filter = 'OR('+filter+')';
	
	console.log("filter:", filter);//////////
	
	return url + `?filterByFormula=${filter}`;
}

function batch(wordsArray) {
	let recordsList = [];
	if (wordsArray.length === undefined || typeof wordsArray === 'string') {wordsArray = [wordsArray]}
	console.log("wordsArray.length:", wordsArray.length, wordsArray);//////////
	if (wordsArray.length > 1) {
		let n;
		let batchArray;
		for (let i = 0; i < wordsArray.length; i += RATE_LIMIT_RR) {
			let j = i + RATE_LIMIT_RR;
			if (j > wordsArray.length) {
				n = wordsArray.length;
			}
			else {
				n = j;
			}
			batchArray = wordsArray.slice(i, n);			
			recordsList.push(batchArray.map((x) => {return {fields: {fldVbAKgn9005YZ0l : String(x)}};}));
		}
	}
	else {
		recordsList.push(wordsArray.map((x) => {return {fields: {fldVbAKgn9005YZ0l : String(x)}};}));
	}
	return recordsList;
}

function batchAndMap(wordsArray, batchSize, mapFunction) {
	let out = [];
	let n;
	let batchArray;
	for (let i = 0; i < wordsArray.length; i += batchSize) {
		let j = i + batchSize;
		if (j > wordsArray.length) {
			n = wordsArray.length;
		}
		else {
			n = j;
		}
		batchArray = wordsArray.slice(i, n);
		out.push(mapFunction(batchArray));
	}
	return out;
}

function arrayOfArrays2array(arrayOfArrays) {
	let outArray = [];
	for (let i = 0; i < arrayOfArrays.length; i++) {
		for (let j = 0; j < arrayOfArrays[i].length; j++) {
			outArray.push(arrayOfArrays[i][j]);
		}
	}
	return outArray;
}
//from https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
//my mod
async function post(request) {
  try {
    const response = await fetch(request);
    const result = await response.json();
    //console.log("Success:", result);
    //my mod
    return result;
  } catch (error) {
    console.error("Error:", error);
    //my mod
    return -1;
  }
}

export {init, INSERT, LOOKUP};
