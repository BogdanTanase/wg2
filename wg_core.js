//Developed by Bogdan Tanase
//June-2024
//update July-2024

var i64b = 2**64;
var ns_max = 20;

var alfa='abcdefghijklmnopqrstuvwxyz';

var beta=[];
for (let i =0; i < alfa.length; i++) {
	for (let j =0; j < alfa.length; j++) {
		beta.push(alfa[i]+alfa[j]);
	}
}

var gama=[];
for (let i =0; i < beta.length; i++) {
	for (let j =0; j < beta.length; j++) {
		gama.push(beta[i]+beta[j]);
	}
}


function valInRHOpenRange(val, minv, maxv) {
	return val >= minv || val < maxv;
}

function generateWord(w_length, distr) {
	//console.log('distr',distr);
	let b_distr;
	let b_distr2;
	let b_distr3;
	let gama_distr;
	for (const d of distr) {
		if (! d.isValid()) {return 'Invalid distr'};
		if (d.id === 1) {b_distr = d};
		if (d.id === 2) {b_distr2 = d};
		if (d.id === 3) {b_distr3 = d};
		if (d.id === 4) {gama_distr = d};
	}
		
	//w2 is the generated word, by adding 2 char elem from beta
	let w2 = '';
	//w4 is last 4 char of generated word
	let w4 = '';
	let ns = 0;
	let r = i64b;
	let ip = i64b;
	let w2o = '';
	if (w_length % 2 == 0) {
		while (w2.length < w_length) {
			r = i64b;
			if (w2 === '') {
				r = weighted_random(rangeArray(beta.length), b_distr.getItem(0));
			}
			else if (w2.length > 3) {
				ip = i64b;
				let ip1 = i64b;
				let ip2 = i64b;
				let ip3 = i64b;
				let ip4 = i64b;
				w4 = w2.slice(-4, w2.length);
				ip1= alfa.indexOf(w4[0]);
				ip2= alfa.indexOf(w4[1]);
				ip3= alfa.indexOf(w4[2]);
				ip4= alfa.indexOf(w4[3]);
				if (ip1 !== i64b && ip2 !== i64b && ip3 !== i64b && ip4 !== i64b) {
					ip = ip1*alfa.length**3 + ip2*alfa.length**2 + ip3*alfa.length + ip4;
				}

				r = getW2FromDistrib(gama_distr, w2o, ns, ip);
			}
			else {r = getW2FromDistrib(b_distr2, w2o, ns);}
			
			if (r !== i64b) {
				w2 += beta[r];
				w2o = beta[r];
			}
			else {
				break;
			}
			ns += 1;
		}
	}
	else {
		while (w2.length < w_length) {
			//console.log('ns: '+ ns);
			r = i64b;
			if (w2 === '') {
				do {
					r = Math.floor(Math.random() * alfa.length);
				} while (b_distr3.validItems.indexOf(r) === -1);
				w2 += alfa[r];
				w2o = alfa[r];
			}
			else {
				ip = i64b;
				if (w2o.length === 1) {
					ip = alfa.indexOf(w2o);
					if (ip !== i64b  && ip !== -1) {
						if (b_distr3.validItems.indexOf(ip) !== -1) {
							r = weighted_random(rangeArray(beta.length), b_distr3.getItem(ip));
						}
						else {
							console.log('case not covered:1')
						}
					}
					else {
						console.log('case not covered:2')
					}
				}
				else {r = getW2FromDistrib(b_distr2, w2o, ns);}			
				
				if (r !== i64b) {
					w2 += beta[r];
					w2o = beta[r];
				}
				else {
					break;
				}
			}
			ns += 1;
		}
	}
	//console.log('w2:: '+ String(w2));
	return w2;
}

function getW2FromDistrib(distrib, w2o, ns, ip = -1) {
	let r = i64b;
	if (ip === -1) {
		ip = beta.indexOf(w2o);
		ip = ip * ns_max + ns;
	
		if (distrib.validItems.indexOf(ip) !== -1) {
			r = weighted_random(rangeArray(beta.length), distrib.getItem(ip));
		}
		else {
			//console.log('case not covered:3')
			
			let n = ns
			
			while(n < ns_max) {
				n += 1;
				//console.log('w2o::ip::n:: '+w2o+' '+ip+' '+n)
				ip = beta.indexOf(w2o);
				ip = ip * ns_max + n;
				if (distrib.validItems.indexOf(ip) !== -1) {
					r = weighted_random(rangeArray(beta.length), distrib.getItem(ip));
					break;
				}
			}
		}
	}
	else {
		if (distrib.validItems.indexOf(ip) !== -1) {
			r = weighted_random(rangeArray(beta.length), distrib.getItem(ip));
		}
	}
	return r;
}

//from https://stackoverflow.com/questions/43566019/how-to-choose-a-weighted-random-array-element-in-javascript
//takes two same-length lists, items and weights, and 
//returns a random item from items according to their corresponding likelihoods of being chosen in weights. 
//The weights array should be of nonnegative numbers, but it doesn't need to sum to a particular value, and
// it can handle non-integer weights.
function weighted_random(items, weights) {
    var i;

    for (i = 1; i < weights.length; i++)
        weights[i] += weights[i - 1];
    
    var random = Math.random() * weights[weights.length - 1];
    
    for (i = 0; i < weights.length; i++)
        if (weights[i] > random)
            break;

    return items[i];
}

//from W3schools
function getRndInteger(min, max) {
	return Math.floor(Math.random() * (Number(max) - Number(min) + 1) ) + Number(min);
}

//
function rangeArray(arrayLength) {
	let rangeArray = [];
	for (let i = 0; i < arrayLength; i++) {
		rangeArray.push(Number(i));
	}
	return rangeArray;
}

//generate
function genWords2(inp_NW, inp_min, inp_max, distr, exclude = []) {
	let word;
	let w_length;
	let word_out = '';
	let words = [];
	
	for (let i = 0; i < inp_NW.value; i++) {
		w_length = getRndInteger(inp_min.value, inp_max.value);
		//console.log('w_length:'+ w_length);
		word = generateWord(w_length, distr);
		while (exclude.indexOf(word) > -1) {
			word = generateWord(w_length, distr);
		}
		word_out += 'w_length:'+ w_length + ' :: ' + word + '\n';
		words.push(word);
		exclude.push(word);
	}
	return [word_out, words];
}

export {generateWord, genWords2};
