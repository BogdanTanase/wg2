async function moduleImport(module) {
	const mod = await import(module)
	return mod;
}

async function main(mod) {
	//console.log('mod : ', mod.value);
	mod = mod.value;
	let p = mod.init();
	
	Promise.allSettled([p]).then(async () => {
		let r0 = mod.LOOKUP('moh');
		r0.then(choseMode);
	});
}

async function choseMode(r) {
	//console.log('connected : ',r);
	if (r === -1) {
		let ui = UI(false);
	}
	else {
		GENERATE_MODE = false;	
		let ui = UI(true);
	}
	mod_wg_data.then(r => {
			const p = r.loadData();
			console.log('p : ',p);
			Promise.allSettled([p]).then((r) => {update_wgUI();});
		});
	
}

function wg(mode) {
	let inp_NW = document.getElementById("inp_NW");
	let inp_min = document.getElementById("inp_min");
	let inp_max = document.getElementById("inp_max");
	if (mode) {
		mod_wg_data.then((r1) => {
			//console.log('r1 ',r1.distr);
			mod_wg_core.then((r) => {wr = r.genWords2(inp_NW, inp_min, inp_max, r1.distr); wordsUI(wr[0])});
		});
	}
	else {
		mod_wg_data.then((r1) => {
			mod_wg_core.then((r) => {wr = r.genWords2(inp_NW, inp_min, inp_max, r1.distr); wordsUI(wr[1]); onlineDb_INSERT(wr[1]);});
		});
	}
}

//onlineDb
async function onlineDb_INSERT(wordsArray) {
	var wordsInDb;
	let p = mod.then(async (r) => {
		wordsInDb = await r.LOOKUP(wordsArray);
		console.log('wordsInDb', wordsInDb);
	});
	
	let p2 = p.then((r) => {
		for (let i = 0; i < wordsInDb.length; i++) {
			wordsArray.splice(wordsArray.indexOf(wordsInDb[i]), 1);
		}
	});
	
	Promise.allSettled([mod, p2]).then(async (r) => {
		console.log('r', r);
		let r3 = await r[0].value.INSERT(wordsArray);
	});
}

function onlineDb_dataCheck(result) {
	//const r = JSON.parse(result);	
	if (result.records.length === 0) {
		console.log('not found in db');
		return false;
	}
	else {
		console.log('found in db : '+String(result.records[0].fields.w));
		return true;
	}
}

//UI
function UI(arg) {
	onlineDbUI(arg);	
	wgUI();	
}

function onlineDbUI(arg) {
	console.log('arg : ', arg);
	
	const para1 = document.createElement("h2");
	para1.setAttribute('id', 'l1');
	
	if (arg) {	
		para1.innerText = "Connected to online database !\n";
		para1.style.color = 'green';	
	}
	else {
		para1.innerText = "Not connected to online database. Working offline !\n";
		para1.style.color = 'blue'}
	document.body.appendChild(para1);
}

function wgUI() {
	const div = document.createElement("div");
	div.setAttribute('id', 'div_wgUI');
	document.body.appendChild(div);	
	
	addButton();
	
	const para = document.createElement("p");
	para.innerText = "Number of words to generate";
	document.body.appendChild(para);

	const inp_NW = document.createElement("input");
	inp_NW.setAttribute('id', 'inp_NW');
	inp_NW.value = 10;
	document.body.appendChild(inp_NW);

	const para2 = document.createElement("p");
	para2.innerText = "min and max word lengh";
	document.body.appendChild(para2);

	const inp_min = document.createElement("input");
	inp_min.setAttribute('id', 'inp_min');
	inp_min.value = 3;
	document.body.appendChild(inp_min);

	const inp_max = document.createElement("input");
	inp_max.setAttribute('id', 'inp_max');
	inp_max.value = 14;
	document.body.appendChild(inp_max);
}

function update_wgUI() {
	const para = document.getElementById("genbutinfo");
	para.innerText = "Ready to generate words !\n";
	para.style.color = 'green'
	
	const btn = document.getElementById("genbut");
	btn.removeAttribute('disabled');
	btn.hidden=false;
}

function wordsUI(words) {
	const div = document.getElementById("div_wgUI");
	
	const hdg = document.createElement("h3");
	hdg.style.color = 'blue'
	hdg.innerText = "Generated words :"
	div.appendChild(hdg);
	
	const para = document.createElement("p");
	para.innerText = words.reduce((x,y) => x+'\n'+y);;
	div.appendChild(para);
	para.scrollIntoView();
}

function addButton() {
	const btn = document.createElement("input");
	btn.type = 'button';
	btn.value = 'Generate';
	btn.setAttribute('id', 'genbut');
	btn.setAttribute('onclick', 'wg(GENERATE_MODE)');
	btn.setAttribute('disabled', '');
	btn.hidden=true;
	document.body.appendChild(btn);
	
	const para = document.createElement("h2");
	para.setAttribute('id', 'genbutinfo');	
	para.innerText = "Wait a while to load data ...\n";
	para.style.color = 'red'
	document.body.appendChild(para);
}

//
var GENERATE_MODE = true;
const mod = moduleImport('./air_wg2.js');
const mod_wg_core = moduleImport('./wg_core.js')
const mod_wg_data = moduleImport('./wg_data.js')
Promise.allSettled([mod, mod_wg_core, mod_wg_data]).then((r) => {
	//console.log('mod_wg_core : ',mod_wg_core, mod_wg_data);
	main(r[0]);
});

